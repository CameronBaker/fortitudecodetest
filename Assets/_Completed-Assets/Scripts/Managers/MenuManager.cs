﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour 
{
	[SerializeField]
	private Complete.GameManager gameManager;
	
	[SerializeField]
	private CanvasGroup menuCanvasGroup;
	[SerializeField]
	private Button lessHumansButton;
	[SerializeField]
	private Button moreHumansButton;
	[SerializeField]
	private Button lessRobotsButton;
	[SerializeField]
	private Button moreRobotsButton;
	[SerializeField]
	private Button playButton;
	[SerializeField]
	private Text playerText;
	[SerializeField]
	private Text robotText;
	

	private int numberOfHumans, numberOfRobots;

	public delegate void OnPlayButtonPressed(int numberOfHumans, int numberOfRobots);
	private OnPlayButtonPressed onPlayButtonPressedCallback;

	public void ShowMenu(OnPlayButtonPressed playPressedCallback)
	{
		menuCanvasGroup.alpha = 1f;

		onPlayButtonPressedCallback = playPressedCallback;
	}

	public void HideMenu()
	{
		menuCanvasGroup.alpha = 0f;

		onPlayButtonPressedCallback = null;
	}

	private void Start()
	{
		//Start with the regular amount of players
		numberOfHumans = 2;
		numberOfRobots = 0;

		//Update text on buttons
		UpdateTextsAndPlayButton();

		//Set up all button events in script rather than inspector.
		lessHumansButton.onClick.AddListener(RemoveHumanPlayers);
		moreHumansButton.onClick.AddListener(AddHumanPlayers);

		lessRobotsButton.onClick.AddListener(RemoveRobotPlayers);
		moreRobotsButton.onClick.AddListener(AddRobotPlayers);

		playButton.onClick.AddListener(PlayButtonPressed);
	}

	private void RemoveHumanPlayers()
	{
		//Clamp to zero
		numberOfHumans = Mathf.Max(0, numberOfHumans - 1);

		UpdateTextsAndPlayButton();
	}

	private void AddHumanPlayers()
	{
		//Clamp to max humans from the game manager
		numberOfHumans = Mathf.Min(gameManager.MaxHumans, numberOfHumans + 1);

		//If there's too many players, remove robots
		if(numberOfHumans + numberOfRobots > gameManager.MaxPlayers)
		{
			RemoveRobotPlayers();
		}

		UpdateTextsAndPlayButton();
	}

	private void RemoveRobotPlayers()
	{
		//Clamp to zero
		numberOfRobots = Mathf.Max(0, numberOfRobots - 1);

		UpdateTextsAndPlayButton();
	}

	private void AddRobotPlayers()
	{
		//Clamp to max robots from the game managers
		numberOfRobots = Mathf.Min(gameManager.MaxRobots, numberOfRobots + 1);

		//If there's too many players, remove humans
		if(numberOfHumans + numberOfRobots > gameManager.MaxPlayers)
		{
			RemoveHumanPlayers();
		}

		UpdateTextsAndPlayButton();
	}

	private void UpdateTextsAndPlayButton()
	{
		playerText.text = numberOfHumans + " Humans";
		robotText.text = numberOfRobots + " Robots";

		//We can only start the game if we have 2 or more players
		playButton.interactable = numberOfHumans + numberOfRobots >= 2;
	}

	private void PlayButtonPressed()
	{
		//Button was pressed, make sure the callback has been set and call it if so
		if(onPlayButtonPressedCallback != null)
		{
			onPlayButtonPressedCallback(numberOfHumans, numberOfRobots);
		}
	}
}
