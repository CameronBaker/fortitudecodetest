﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Complete;

public class AITankShooting : BaseTankShooting 
{
	public bool ChargeCannon()
	{
		if(IsFiring)
		{
			return false; //Already firing, can't fire again.
		}

		StartFire();
		return true;
	}

	public bool FireCannon()
	{
		if(!IsFiring)
		{
			return false; //Haven't begun charging the cannon yet.
		}

		EndFire();
		return true;
	}

	public bool UnloadCannon()
	{
		if(!IsFiring)
		{
			return false; //Nothing to unload
		}

		CancelFire();
		return true;
	}

	protected override void Start()
	{
		base.Start();
	}

	protected override void Update()
	{
		base.Update();
    }

    public Vector3 GetExpectedProjectileHitPoint(float launchForce)
	{
		//Get the expected distance the projectile will travel given the current force
        var horizontalDisplacement = GetExpectedProjectileDistance(launchForce);

        //Now figure out where the final landing position is
        var flatForward = m_FireTransform.forward;
		flatForward.y = 0f;
		flatForward.Normalize();

		return m_FireTransform.position + flatForward * horizontalDisplacement;
	}

	public float GetExpectedProjectileDistance(float launchForce)
	{
		//Calculate time of flight for the given projectile 
        var shellLaunchAngle = 360f - m_FireTransform.localEulerAngles.x; //Reads as 350 because angles count clockwise, we want angles to count counter-clockwise, so negate from 360 
        var constantVerticalVelocity = launchForce * Mathf.Sin(shellLaunchAngle * Mathf.Deg2Rad); //launchVelocity * sin(launchAngle) 
        var constantHorizontalVelocity = launchForce * Mathf.Cos(shellLaunchAngle * Mathf.Deg2Rad); //launch velocity * cos(launchAngle)
        var verticalDisplacement = m_FireTransform.localPosition.y * -1f; //height of the firing transform above the ground (negated so our equation knows it's a negative offset)
        var gravity = Physics.gravity.y;

        //Skip the displacement formula work ang go straight to the final derivation.
        //Plug these values into the quadratic equation to get time of flight (use the usual a,b,c to make the equation easier to read)
        var a = gravity / 2f;
        var b = constantVerticalVelocity; 
        var c = verticalDisplacement * -1f; //becomes a positive number as it progresses through the displacement formula

        var timeOfFlight = ((b * -1f) - Mathf.Sqrt((b * b) - (4f * a * c))) / (2f * a); //Quadratic equation (-B - SQRT (B^2 - 4AC)) / 2A

        //Now we have time of flight, we can work out horizontal displacement
        var horizontalDisplacement = timeOfFlight * constantHorizontalVelocity;

		return horizontalDisplacement;
	}
}
