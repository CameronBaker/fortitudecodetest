﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Complete;

[RequireComponent(typeof(Rigidbody))]
public class AITankMovement : BaseTankMovement
{	
	[SerializeField]
	[Tooltip("If next waypoint is above this degree threshold, unit will turn on the spot rather than moving straight away")]
	private float tankTurnOnSpotThreshold = 30f;

	private List<Vector3> currentPath = new List<Vector3>(); // Current path as vector3 points
	private bool tankMovingInReverse; //Keep track of wether we're moving in reverse or not. Set in 'turn along path'
	private float lastDeltaAngle; //Keep track of the last angle between our forward and the next path point. Set in 'turn along path'

	public void SetCurrentPath(Vector3[] path)
	{
		//Pass in a new path for us to follow
		currentPath.Clear();
		currentPath.AddRange(path);
	}

	public bool HasValidPath()
	{
		return currentPath.Count != 0;
	}

	public void ClearPathTargetsThatAreCloseToUs()
	{
		//See if we're already at one of the path points, if so, remove it
		for(int i = currentPath.Count - 1; i >= 0; i--)
		{
			var toPathPoint = currentPath[i] - m_Rigidbody.position;

			if(toPathPoint.sqrMagnitude < 0.5f * 0.5f) //Check to see wether it's within half a unit
			{
				currentPath.RemoveAt(i);
			}
		}
	}

	public void RotateToTarget(Vector3 targetPosition)
	{
		//Get the vector to the target
		var toTarget = targetPosition - m_Rigidbody.position;
		//Get the normalised direction to the target
		var directionToTarget = toTarget.normalized;

		//Rotate the tank towards the new heading
		var currentForwardAngle = Mathf.Atan2(transform.forward.z, transform.forward.x) * Mathf.Rad2Deg;
		var aimAngle = Mathf.Atan2(directionToTarget.z, directionToTarget.x) * Mathf.Rad2Deg;
		var deltaAngle = Mathf.DeltaAngle(currentForwardAngle, aimAngle);

		//Turn by turn speed unless it is greater than the angular difference, turn by that value instead
		var turnAmount = Mathf.Clamp(Mathf.Sign(deltaAngle) * -1f * m_TurnSpeed * Time.fixedDeltaTime, Mathf.Abs(deltaAngle) * -1f, Mathf.Abs(deltaAngle));
		
		TurnByAngle(turnAmount);
	}

	public void FollowPath(bool allowReverseMovement)
	{
		TurnAlongPath(allowReverseMovement);
		MoveAlongPath(allowReverseMovement);
	}

	protected override void OnEnable()
	{
		base.OnEnable();

		tankMovingInReverse = false;
		lastDeltaAngle = 0f;
	}
	protected override void OnDisable()
	{
		base.OnDisable();
	}

	protected override void FixedUpdate()
	{
		base.FixedUpdate();
	}

	private void TurnAlongPath(bool allowReverseMovement)
	{
		//Bail out if we don't have a valid path
		if(currentPath.Count == 0)
		{
			return;
		}

		//Get the vector to the first point in the path
		var toFirstPointInPath = currentPath[0] - m_Rigidbody.position;

		//Get the normalised direction to the path point
		var directionToFirstPoint = toFirstPointInPath.normalized;

		//Rotate the tank towards the new heading
		var currentForwardAngle = Mathf.Atan2(transform.forward.z, transform.forward.x) * Mathf.Rad2Deg;
		var currentReverseAngle = Mathf.Atan2(transform.forward.z * -1f, transform.forward.x * -1f) * Mathf.Rad2Deg;
		var aimAngle = Mathf.Atan2(directionToFirstPoint.z, directionToFirstPoint.x) * Mathf.Rad2Deg;
		var deltaAngle = Mathf.DeltaAngle(currentForwardAngle, aimAngle);

		//If we're allowing reverse movement, check if it's quicker to rotate our back to the direction and start moving
		tankMovingInReverse = false;
		if(allowReverseMovement && Mathf.Abs(deltaAngle) > 90f)
		{
			deltaAngle = Mathf.DeltaAngle(currentReverseAngle, aimAngle);
			tankMovingInReverse = true;
		}

		//Turn by turn speed unless it is greater than the angular difference, turn by that value instead
		var turnAmount = Mathf.Clamp(Mathf.Sign(deltaAngle) * -1f * m_TurnSpeed * Time.fixedDeltaTime, Mathf.Abs(deltaAngle) * -1f, Mathf.Abs(deltaAngle));
		
		TurnByAngle(turnAmount);

		lastDeltaAngle = deltaAngle;
	}
		
	private void MoveAlongPath(bool allowReverseMovement)
	{
		//Bail out if we don't have a valid path
		if(currentPath.Count == 0)
		{
			return;
		}

		//Get the vector to the first point in the path
		var toFirstPointInPath = currentPath[0] - m_Rigidbody.position;

		//Only allow movement if we are angled within the threshold
		if( Mathf.Abs(lastDeltaAngle) < tankTurnOnSpotThreshold )
		{
			// Apply movement over the tanks forward or backward vector. Clamp by the distance to the target so we don't get stuttering by overshooting
			var distanceToFirstPoint = toFirstPointInPath.magnitude;
			var movementDistance = Mathf.Clamp(m_Speed * Time.fixedDeltaTime, 0f, distanceToFirstPoint);
			Vector3 movement = (tankMovingInReverse ? transform.forward * -1f : transform.forward) * movementDistance;

			// Apply this movement to the rigidbody's position.
			MoveByAmount(movement);
		}

		//Draw path as line in debug
		// Debug.DrawLine(m_Rigidbody.position, currentPath[0], Color.blue);
		// for(int i = 0; i < currentPath.Count - 1; i++)
		// {
		// 	Debug.DrawLine(currentPath[i], currentPath[i+1], Color.blue);
		// }
	}
}
