﻿using UnityEngine;
using UnityEngine.UI;
using Complete;

public class PlayerTankShooting : BaseTankShooting
{
    private string m_FireButton;                // The input axis that is used for launching shells.



	private Vector3 lastShellPos;

    protected override void Start ()
    {
        base.Start();

        // The fire axis is based on the player number.
        m_FireButton = "Fire" + m_PlayerNumber;
    }

    protected override void Update()
    {
        base.Update();

        if (Input.GetButton (m_FireButton) && !IsFiring)
        {
            StartFire();
        }
        else if (Input.GetButtonUp (m_FireButton))
        {
            EndFire();
        }
    }
}