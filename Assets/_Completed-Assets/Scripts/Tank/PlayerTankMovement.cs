﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Complete;

public class PlayerTankMovement : BaseTankMovement
{
	private string m_MovementAxisName;          // The name of the input axis for moving forward and back.
	private string m_TurnAxisName;              // The name of the input axis for turning.
	protected float m_MovementInputValue;         // The current value of the movement input.
	protected float m_TurnInputValue;             // The current value of the turn input.

	protected override void Start()
	{
		// The axes names are based on player number.
		m_MovementAxisName = "Vertical" + m_PlayerNumber;
		m_TurnAxisName = "Horizontal" + m_PlayerNumber;
	}

	protected override void Update()
	{
		base.Update();
		
		// Store the value of both input axes.
		m_MovementInputValue = Input.GetAxis (m_MovementAxisName);
		m_TurnInputValue = Input.GetAxis (m_TurnAxisName);
	}

	protected override void OnEnable()
	{
		// Also reset the input values.
		m_MovementInputValue = 0f;
		m_TurnInputValue = 0f;
	}

	protected override void OnDisable()
	{

	}

	protected override void FixedUpdate ()
	{
		base.FixedUpdate();

		// Adjust the rigidbodies position and orientation in FixedUpdate.
		Move ();
		Turn ();
	}


	private void Move ()
	{
		// Create a vector in the direction the tank is facing with a magnitude based on the input, speed and the time between frames.
		Vector3 movement = transform.forward * m_MovementInputValue * m_Speed * Time.deltaTime;

		MoveByAmount(movement);
	}


	private void Turn ()
	{
		// Determine the number of degrees to be turned based on the input, speed and time between frames.
		float turn = m_TurnInputValue * m_TurnSpeed * Time.deltaTime;

		TurnByAngle(turn);
	}
}
