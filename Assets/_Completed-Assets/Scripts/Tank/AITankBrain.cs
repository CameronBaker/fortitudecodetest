﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Complete;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(AITankShooting))]
[RequireComponent(typeof(AITankMovement))]
[RequireComponent(typeof(TankHealth))]
public class AITankBrain : MonoBehaviour 
{
	[HideInInspector]
	public BoxCollider NavMeshBounds;	//Used to determine a random point to roam to. It will keep new random points within these bounds
	[HideInInspector]
	public List<GameObject> allTanks;	//All tanks on the field bar us, alive or dead

	[SerializeField]
	[Range(0f, 1f)]
	[Tooltip("How likley the tank will attack when they're hunting. The inverse is how likely the tank will evade when targeted")]
	private float tankAgression = 0.5f;
	
	[SerializeField]
	[Range(0f, 1f)]
	[Tooltip("Distance the tank will keep between it and it's target when hunting or evading. This is a percetange of fire distance")]
	private float huntOffset = 0.5f;

	[SerializeField]
	[Range(0f, 180f)]
	[Tooltip("Line of sight cone angle when the tank ISN'T currently chasing a target")]
	private float unawareLineOfSightAngle = 30f;
	[SerializeField]
	[Range(0f, 180f)]
	[Tooltip("Line of sight cone angle when the tank IS chasing a target")]
	private float awareLineOfSightAngle = 90f;

	[SerializeField]
	[Range(0f, 100f)]
	[Tooltip("Line of sight cone length when the tank ISN'T currently chasing a target")]
	private float unawareLineOfSightDistance = 20f;
	[SerializeField]
	[Range(0f, 100f)]
	[Tooltip("Line of sight cone length when the tank IS chasing a target")]
	private float awareLineOfSightDistance = 40f;

	[SerializeField]
	[Tooltip("How long a targeted tank needs to be out of line of sight to get lost")]
	private float targetLossTime = 3f;

	[SerializeField]
	[Tooltip("The minimum amount of time a unit will evade if the enemy is firing")]
	private float minEvadeTime = 4f;


	private Rigidbody rigidbodyReference;
	private AITankShooting tankShooting;
	private AITankMovement tankMovement;
	private Complete.TankHealth tankHealth;

	private GameObject chaseTarget;
	private BaseTankShooting chaseTargetCannon;

	private float hoverDistanceFromTarget;
	private bool evading = false;
	private float loseTargetTimer = 0f;
	private float evadeTimer = 0f;
	private Vector3 lastChaseTargetPosition;
	private Vector3 expectedPositionOfChaseTarget;

	private Rigidbody RigidbodyReference 
	{ get {
		return rigidbodyReference = rigidbodyReference == null ? GetComponent<Rigidbody>() : rigidbodyReference;
	}}

	private AITankMovement TankMovement
	{ get {
		return tankMovement = tankMovement == null ? GetComponent<AITankMovement>() : tankMovement;
	}}

	private AITankShooting TankShooting
	{ get {
		return tankShooting = tankShooting == null ? GetComponent<AITankShooting>() : tankShooting;
	}}

	private Complete.TankHealth TankLife
	{ get {
		return tankHealth = tankHealth == null ? GetComponent<Complete.TankHealth>() : tankHealth;
	}}

	private void Awake()
	{
		//Keep a reference to the mid distance a projectile will travel. use this when determining hunt offsets f
		var hoverDistAsPower = Mathf.Lerp(TankShooting.m_MinLaunchForce,TankShooting.m_MaxLaunchForce, huntOffset);
		hoverDistanceFromTarget = TankShooting.GetExpectedProjectileDistance(hoverDistAsPower);
	}

	private void OnEnable()
	{
		TankLife.OnDamageTaken = HandleOnDamageTaken; //Set the callback for the damage taken event
	}

	private void OnDisable()
	{
		TankLife.OnDamageTaken = null;	//Clear the callback when we're disabled
	}

	private void FixedUpdate()
	{
		BehaviourLoop(); //Behaviour loop runs each fixed frame
	}

	private void BehaviourLoop()
	{
		UpdateTargetStatus(); 	//Update the target status to see if we have a target or found one

		if(chaseTarget == null) //Still didn't find anything
		{
			Roam(); 			//Roam around until we stop something
		}
		else
		{
			//Get the expected position and smooth it to reduce some jerky motion
			expectedPositionOfChaseTarget = Vector3.Lerp(expectedPositionOfChaseTarget, GetExpectedChaseTargetPosition(), Time.fixedDeltaTime * 12f);

			//Draw the current expected position in debug view
			//Debug.DrawRay(expectedPositionOfChaseTarget, Vector3.up, Color.magenta);

			UpdateEvadeState(); 		//Check if we should start or stop evasion

			if(evading) 				//Already evading
			{
				Evade();				//Continue to evade
			}
			else
			{
				Hunt();					//Continue hunting
				UpdateFireState();		//Try to fire if our agression is high enough
			}

			lastChaseTargetPosition = chaseTarget.transform.position; //Cache the last position of the target for use in deriving velocity
		}
	}

	private void UpdateTargetStatus()
	{
		if(chaseTarget == null) //No target to chase
		{
			chaseTarget = GetFirstUnitInLineOfSight(); //See if anything is within our line of sight cone

			loseTargetTimer = 0f;

			if(chaseTarget != null) //Assign the chase target cannon to a reference
			{
				chaseTargetCannon = chaseTarget.GetComponent<BaseTankShooting>();

				lastChaseTargetPosition = expectedPositionOfChaseTarget = chaseTarget.transform.position; //Cache it's current position & make it the expected position
			}
		}
		else	//We have a target, but are they still in range or alive?
		{
			if(!chaseTarget.activeSelf)
			{
				//target is dead. Clear it out
				
				chaseTarget = null;	//Clear the target
				return; //Skip the line of sight checks
			}

			if(IsUnitWithinLineOfSight(chaseTarget, true))
			{
				loseTargetTimer = 0f; //Still within line of sight, reset the timer
			}
			else
			{
				if(loseTargetTimer >= targetLossTime)	//Not in line of sight and out of time
				{
					chaseTarget = null;	//Clear the target
				}

				loseTargetTimer += Time.fixedDeltaTime; //Add to the timer
			}
		}
	}

	private GameObject GetFirstUnitInLineOfSight()
	{
		//Check each tank on the field if it's alive. If it's in line of sight, return it as our target
		foreach(var tank in allTanks)
		{
			if(tank.activeSelf)
			{
				if(IsUnitWithinLineOfSight(tank, false))
				{
					return tank;
				}
			}
		}

		return null;
	}

	private bool IsUnitWithinLineOfSight(GameObject target, bool aware)
	{
		if(!target.activeSelf)
		{
			return false; //Target not active, return false
		}

		//Get appropriate length and angle for aware state
		var coneAngle = aware ? awareLineOfSightAngle : unawareLineOfSightAngle;
		var coneLength = aware ? awareLineOfSightDistance : unawareLineOfSightDistance;

		//First check if we are within distance
		var toTarget = target.transform.position - RigidbodyReference.position;

		if(toTarget.sqrMagnitude <= coneLength * coneLength)
		{
			//Now check wether we're within the cone angle
			toTarget.Normalize();
			var currentForwardAngle = Mathf.Atan2(transform.forward.z, transform.forward.x) * Mathf.Rad2Deg;
			var toTargetAngle = Mathf.Atan2(toTarget.z, toTarget.x) * Mathf.Rad2Deg;
			var deltaAngle = Mathf.DeltaAngle(currentForwardAngle, toTargetAngle);

			if(Mathf.Abs(deltaAngle) < coneAngle)
			{
				return true;
			}
		}

		return false;
	}

	private void UpdateEvadeState()
	{
		if(evading)
		{
			if(CheckToStopEvade()) //If the target has stopped trying to fire, stop evading
			{
				evading = false;
			}
			else
			{
				evadeTimer += Time.fixedDeltaTime;
			}
		}
		else
		{
			if(CheckToStartEvade()) //Evade if our agression is low and the target is firing at us
			{
				evading = true;
				evadeTimer = 0f;
				TankShooting.UnloadCannon(); //Stop firing if we move to evade
			}
		}
	}

	private bool CheckToStartEvade()
	{
		if(chaseTargetCannon.IsFiring)
		{
			//Check if the target is currently facing us.
			var dirToUs = (RigidbodyReference.position - chaseTarget.transform.position).normalized;

			if(Vector3.Dot(dirToUs, chaseTarget.transform.forward) > 0.6f && Random.value < ((1f - tankAgression))) //Not checked every frame so don't multply by Dt
			{
				return true;
			}
		}

		return false;
	}

	private bool CheckToStopEvade()
	{
		//Target not firing and we're out of min evade time
		return (!chaseTargetCannon.IsFiring && evadeTimer >= minEvadeTime);
	}

	private void UpdateFireState()
	{
		if(!TankShooting.IsFiring)
		{
			if(CheckChargeCannonAvailable()) //We aren't firing and have a viable target
			{
				TankShooting.ChargeCannon();
			}
		}
		else
		{
			if(CheckFireCannonAvailable()) //We are firing and our target is in range
			{
				TankShooting.FireCannon();
			}
		}
	}

	private bool CheckChargeCannonAvailable()
	{
		var distanceToTarget = Vector3.Distance(RigidbodyReference.position, expectedPositionOfChaseTarget);

		//If we aren't shooting and we're closer than the max launch force, than start charging.
		if(!TankShooting.IsFiring && distanceToTarget <= TankShooting.GetExpectedProjectileDistance(TankShooting.m_MaxLaunchForce) && Random.value < tankAgression * Time.fixedDeltaTime) //Checked every frame so multiply agression by Dt
		{
			return true;
		}

		return false;
	}

	private bool CheckFireCannonAvailable()
	{
		var distanceToTarget = Vector3.Distance(RigidbodyReference.position, expectedPositionOfChaseTarget);

		//If we're charging and the expected landing spot is within a unit of the target then fire!
		var landingSpotToTargetDistance = Mathf.Abs(distanceToTarget - TankShooting.GetExpectedProjectileDistance(TankShooting.CurrentLaunchForce));
		if(TankShooting.IsFiring && landingSpotToTargetDistance < 1f)
		{
			return true;
		}

		return false;
	}

	private void Hunt()
	{
		//Chase down the target and face it's expected position

		//Create a new path each frame. If we can't, keep using the old path until a new path is ready
		NavMeshPath newPath = new NavMeshPath();
		if(FindHuntPath(ref newPath, expectedPositionOfChaseTarget))
		{
			TankMovement.SetCurrentPath(newPath.corners);
		}

		TankMovement.ClearPathTargetsThatAreCloseToUs();

		if(TankMovement.HasValidPath())
		{
			//Only allow moving backwards if we're facing the target.
			var forwardDot = Vector3.Dot(transform.forward, (expectedPositionOfChaseTarget - RigidbodyReference.position).normalized);

			TankMovement.FollowPath(forwardDot > 0.5f); //Can only move backwards if you're within 45 degrees of the target
		}
		else
		{
			TankMovement.RotateToTarget(expectedPositionOfChaseTarget);
		}
	}

	private void Roam()
	{
		//Move to a random point on the map

		if(!TankMovement.HasValidPath()) //No path, make a new one.
		{
			NavMeshPath newPath = new NavMeshPath();
			if(FindRoamingPath(ref newPath))
			{
				TankMovement.SetCurrentPath(newPath.corners);
			}
		}

		TankMovement.ClearPathTargetsThatAreCloseToUs();

		TankMovement.FollowPath(false); 
	}

	private void Evade()
	{
		//Go around the target in a circle to avoid fire

		if(!TankMovement.HasValidPath()) //No path, make a new one.
		{
			NavMeshPath newPath = new NavMeshPath();
			if(FindEvadePath(ref newPath))
			{
				TankMovement.SetCurrentPath(newPath.corners);
			}
		}

		TankMovement.ClearPathTargetsThatAreCloseToUs();

		TankMovement.FollowPath(true); 
	}

	private bool FindEvadePath(ref NavMeshPath navigationPath)
	{
		//Get the vector to the target, a normalised direction and an angle
		var fromTarget = RigidbodyReference.position - chaseTarget.transform.position;
		var directionfromTarget = fromTarget.normalized;
		var fromTargetAngle = Mathf.Atan2(directionfromTarget.z, directionfromTarget.x) * Mathf.Rad2Deg;

		//Evading will be similar to cirle strafing (minus the strafing part). Tank will just circle the player to stay in range but dodge incoming attacks.
		//Pick a random angle between a min and max, then give it a chance to be a negative change
		var angleOffset = Random.Range(22f, 45f) * (Random.value > 0.75f ? -1f : 1f);
		
		//Add the offset back to the target angle and create a new forward vector from that
		var newTargetAngle = fromTargetAngle + angleOffset;
		var newTargetDirection = new Vector3(Mathf.Cos(newTargetAngle * Mathf.Deg2Rad), 0f, Mathf.Sin(newTargetAngle * Mathf.Deg2Rad));

		//Create a new position using the new target direction and keep it the mid shot distance just to ensure the tank stays close enough to fire when it wants
		var evadePosition = chaseTarget.transform.position + newTargetDirection * hoverDistanceFromTarget;

		return FindPathForTarget(ref navigationPath, evadePosition);
	}

	private bool FindRoamingPath(ref NavMeshPath navigationPath)
	{
		//Only run this a maximum on 10 times in a frame to avoid expense. If we can't find a path, wait until next frame.
		var findCounter = 10;
		while(findCounter > 0)
		{
			var randomLocation = GetRandomLocation();

			if(FindPathForTarget(ref navigationPath, randomLocation))
			{
				return true; //We have a valid path, return true!
			}

			findCounter--; //No valid path yet, loop until we run out of tries.
		}

		return false; //Couldn't find a valid path, return false
	}

	private bool FindHuntPath(ref NavMeshPath navigationPath, Vector3 huntTargetPosition)
	{
		var huntOffset = hoverDistanceFromTarget;
		if(TankShooting.IsFiring)
		{
			//Instead of using the hunt offset, calculate the projectile distance and use that as the offset
			huntOffset = TankShooting.GetExpectedProjectileDistance(TankShooting.CurrentLaunchForce);
		}

		var directionFromTargetToUs = RigidbodyReference.position - huntTargetPosition;
		var huntPosition =  huntTargetPosition + directionFromTargetToUs.normalized * huntOffset;

		return FindPathForTarget(ref navigationPath, huntPosition);
	}

	private bool FindPathForTarget(ref NavMeshPath navigationPath, Vector3 targetPosition)
	{
		var resultingLocation = Vector3.zero;
		if(GetClosestPointOnNavMesh(out resultingLocation, targetPosition))
		{
			//Only generate the path if we have a viable location
			if(NavMesh.CalculatePath(RigidbodyReference.position, resultingLocation, NavMesh.AllAreas, navigationPath))
			{
				return true; //We have a valid path, return true!
			}
		}
		return false;
	}

	private Vector3 GetExpectedChaseTargetPosition()
	{
		//Get the velocity of the chase target
		var perFrameVelocity = chaseTarget.transform.position - lastChaseTargetPosition;
		var perSecondVelocity = perFrameVelocity * (1f / Time.fixedDeltaTime);

		// We know the shell speed per second, so get the distance of the target, firgure out how many seconds it would take for the projectile
		// to reach it and then use that amount to offset the aim target 
		var toTarget = chaseTarget.transform.position - RigidbodyReference.position;
		var distToTarget = toTarget.magnitude;
		var timeToTravel = distToTarget	 / TankShooting.CurrentLaunchForce;

		//Our offset is the velocity over the time to travel
		var offset = perSecondVelocity * timeToTravel;

		return chaseTarget.transform.position + offset;
	}

	private Vector3 GetRandomLocation()
	{
		//Start random position at the center of the collider. Add the world pos of the transform to the local center of the collider.
		var randomLocation = NavMeshBounds.transform.position + NavMeshBounds.center; 
		randomLocation.x = NavMeshBounds.bounds.extents.x * Random.Range(-1f, 1f);
		randomLocation.z = NavMeshBounds.bounds.extents.z * Random.Range(-1f, 1f);

		return randomLocation;
	}

	private bool GetClosestPointOnNavMesh(out Vector3 resultingLocation, Vector3 originLocation)
	{
		//Get the closest point on the nav mesh. If we can't find anything, return false.
		NavMeshHit navMeshHit;
		if(NavMesh.SamplePosition(originLocation, out navMeshHit, 2f, NavMesh.AllAreas))
		{
			resultingLocation = navMeshHit.position;
			return true;
		}

		resultingLocation = Vector3.zero;
		return false;
	}

	private void HandleOnDamageTaken(Vector3 position, Vector3 direction)
	{
		if(chaseTarget == null) //We have no target but have taken damage, someone is following us
		{
			//Make a path towards the direction of the shot to see if we can spot the enemy
			var damagePosition = position + direction * -10f;

			NavMeshPath newPath = new NavMeshPath();
			if(FindPathForTarget(ref newPath, damagePosition))
			{
				TankMovement.SetCurrentPath(newPath.corners);
			}
		}
	}
}