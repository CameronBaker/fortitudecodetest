﻿using UnityEngine;

namespace Complete
{
    //Base class mostly handles sound and particle effect behaviour and also stores motion settings.
    public class BaseTankMovement : MonoBehaviour
    {
        public int m_PlayerNumber = 1;              // Used to identify which tank belongs to which player.  This is set by this tank's manager.
        public float m_Speed = 12f;                 // How fast the tank moves forward and back.
        public float m_TurnSpeed = 180f;            // How fast the tank turns in degrees per second.
        public AudioSource m_MovementAudio;         // Reference to the audio source used to play engine sounds. NB: different to the shooting audio source.
        public AudioClip m_EngineIdling;            // Audio to play when the tank isn't moving.
        public AudioClip m_EngineDriving;           // Audio to play when the tank is moving.
		public float m_PitchRange = 0.2f;           // The amount by which the pitch of the engine noises can vary.

        protected Rigidbody m_Rigidbody;              // Reference used to move the tank.
        private float m_OriginalPitch;              // The pitch of the audio source at the start of the scene.
        private ParticleSystem[] m_particleSystems; // References to all the particles systems used by the Tanks
        private Vector3 lastPosition, lastRotation; // Store last position and rotation so we can work out motion for use in sound effects

        protected virtual void Awake ()
        {
            m_Rigidbody = GetComponent<Rigidbody> ();
        }


        protected virtual void OnEnable ()
        {
            // When the tank is turned on, make sure it's not kinematic.
            m_Rigidbody.isKinematic = false;

            // We grab all the Particle systems child of that Tank to be able to Stop/Play them on Deactivate/Activate
            // It is needed because we move the Tank when spawning it, and if the Particle System is playing while we do that
            // it "think" it move from (0,0,0) to the spawn point, creating a huge trail of smoke
            m_particleSystems = GetComponentsInChildren<ParticleSystem>();
            for (int i = 0; i < m_particleSystems.Length; ++i)
            {
                m_particleSystems[i].Play();
            }
        }


        protected virtual void OnDisable ()
        {
            // When the tank is turned off, set it to kinematic so it stops moving.
            m_Rigidbody.isKinematic = true;

            // Stop all particle system so it "reset" it's position to the actual one instead of thinking we moved when spawning
            for(int i = 0; i < m_particleSystems.Length; ++i)
            {
                m_particleSystems[i].Stop();
            }
        }


        protected virtual void Start ()
        {
            // Store the original pitch of the audio source.
            m_OriginalPitch = m_MovementAudio.pitch;
        }


        protected virtual void Update ()
        {
            EngineAudio ();

            lastPosition = m_Rigidbody.position;
            lastRotation = transform.eulerAngles;
        }

        protected virtual void FixedUpdate()
        {

        }

        protected void MoveByAmount(Vector3 motion)
        {
            // Apply this movement to the rigidbody's position.
		    m_Rigidbody.MovePosition(m_Rigidbody.position + motion);
        }

        protected void TurnByAngle(float angle)
        {
            // Make this into a rotation in the y axis.
            Quaternion turnRotation = Quaternion.Euler (0f, angle, 0f);

            // Apply this rotation to the rigidbody's rotation.
            m_Rigidbody.MoveRotation (m_Rigidbody.rotation * turnRotation);
        }

        private void EngineAudio ()
        {
            var movement = m_Rigidbody.position - lastPosition;
            var rotation = transform.eulerAngles - lastRotation;

            // If there is no input (the tank is stationary)...
            if ( Mathf.Approximately(movement.sqrMagnitude, 0) && Mathf.Approximately(rotation.sqrMagnitude, 0))
            {
                // ... and if the audio source is currently playing the driving clip...
                if (m_MovementAudio.clip == m_EngineDriving)
                {
                    // ... change the clip to idling and play it.
                    m_MovementAudio.clip = m_EngineIdling;
                    m_MovementAudio.pitch = Random.Range (m_OriginalPitch - m_PitchRange, m_OriginalPitch + m_PitchRange);
                    m_MovementAudio.Play ();
                }
            }
            else
            {
                // Otherwise if the tank is moving and if the idling clip is currently playing...
                if (m_MovementAudio.clip == m_EngineIdling)
                {
                    // ... change the clip to driving and play.
                    m_MovementAudio.clip = m_EngineDriving;
                    m_MovementAudio.pitch = Random.Range(m_OriginalPitch - m_PitchRange, m_OriginalPitch + m_PitchRange);
                    m_MovementAudio.Play();
                }
            }
        }


        
    }
}